import express, { Request, Response } from 'express';

export const eventRouter = express.Router();

eventRouter.get('/', (req: Request, res: Response) => { res.send(`Event list`); });
eventRouter.get('/:eventId([0-9]+)', (req: Request, res: Response) => { res.send(`Event Info ${req.params.eventId}`); });
