import express, { Request, Response } from 'express';

export const userRouter = express.Router();

userRouter.get('/', (req: Request, res: Response) => { res.send(`User list`); });
userRouter.get('/:userId([0-9]+)', (req: Request, res: Response) => { res.send(`User Info ${req.params.userId}`); });
