# Express

## Getting started

```
git clone git@gitlab.com:node_irn/express_with_mongodb.git
cd express_with_mongodb
npm i
npm run dev
```

## App URL

[http://localhost:3000](http://localhost:3000)
