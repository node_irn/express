import express, { Express, NextFunction, Request, Response } from 'express';
import moment from 'moment';
import dotenv from 'dotenv';
import { userRouter } from './routes/users';
import { eventRouter } from './routes/events';

dotenv.config();

const app: Express = express();
const port = process.env.APP_PORT;

app.use('/', (req: Request, res: Response, next: NextFunction) => {
  console.log("API call received", moment(Date.now()).format("dddd, MMMM Do YYYY, h:mm:ss a"))
  next();
});

app.use('/user', userRouter);
app.use('/event', eventRouter);
app.use('/', (req: Request, res: Response, next: NextFunction) => {
  res.send(`Express + TypeScript Server`);
  next();
});

app.use('/', (req: Request, res: Response, next: NextFunction) => {
  console.log("API call ended", moment(Date.now()).format("dddd, MMMM Do YYYY, h:mm:ss a"));
});

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});